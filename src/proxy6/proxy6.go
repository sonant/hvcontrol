package proxy6

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"github.com/labstack/gommon/log"
	"github.com/Jeffail/gabs"
)

var api_url string = "https://proxy6.net/api/"

type Proxy struct {
	Api_key string
}

type ProxyIteam struct{
		Id string
		Ip string
		Port string
		User string
		Pass string
		Active string
		Date string
		Date_end string
		Descr string
		Version string
}


type GetCountyResponse struct {
	Status string
	User_id string
	Balance string
	Currency string
	List []string
}

type BuyResponse struct {
	Status string
	User_id string
	Balance string
	Currency string
	Count int
	Price float32
	Price_single float32
	Period int
	County string
	List []ProxyIteam
}

type GetProxyResponse struct {
	Status string
	User_id string
	Balance string
	Currency string
	List_count int
	List ProxyIteam
}

func(c *Proxy) GetCounty() GetCountyResponse {
	r, err := http.Get(api_url+c.Api_key+"/getcountry?version=4")
	if err!=nil {
		log.Fatal(err.Error())
	}
	defer r.Body.Close()
	res, err := ioutil.ReadAll(r.Body)
	var s GetCountyResponse
	if err:=json.Unmarshal(res,&s);err!=nil{
		log.Fatal(err.Error())
	}
	return s
}

func(c *Proxy) GetProxy() []ProxyIteam {
	var ProxyList = []ProxyIteam{}
	r, err := http.Get(api_url+c.Api_key+"/getproxy")
	if err!=nil {
		log.Fatal(err.Error())
	}
	defer r.Body.Close()
	res, err := ioutil.ReadAll(r.Body)
	parsed, _:= gabs.ParseJSON(res)
	x,_:=parsed.S("list").Children()
	var s ProxyIteam
	for _,z := range x{
		if err:=json.Unmarshal(z.Bytes(),&s);err!=nil{
			log.Fatal(err.Error())
		}
		ProxyList=append(ProxyList,s)
	}
	return ProxyList

}

func (c *Proxy) Buy(count, period, version int, country, descr string) (BuyResponse,error){
	resp:=BuyResponse{}
	return resp,nil
}