Vue.component('modal', {
  data:function(){return{
  	timezone:'',
	language:'',
	vm_name:''
  }},
  template: '#modal-template',
  methods:{
	create_vm:function(event){
				if(this.vm_name!=''){
					console.log(this.vm_name);
					fetch('/vm_create/'+this.vm_name+'/'+this.timezone+'/'+this.language)
					.then((response)=>{	if(response.ok){return response.text();}			
						throw new Error('Network response error');})		
					.then((text)=>{
						app.load_data();
					})
					.catch((error)=>{
						console.log(error);
					});
				
				}
	}
  }
});
Vue.component('vm_delete',{
	props:['vmname'],
	template:'<button v-on:click="vm_delete">DELETE</button>',
	methods:{	
			vm_delete:function(event){ 
				if(confirm('Delete vm?')){
				console.log("VM delete! "+this.vmname);
				fetch('/vm_delete/'+this.vmname)
				.then((response)=>{
					if(response.ok){
						return response.text();
					}
					throw new Error('Network response error');
				})
				.then((text)=>{					
					console.log(text);
					app.load_data();
				})
				.catch((error)=>{
					console.log(error);
				});			
			}
			},
	},
});

Vue.component('vm_up',{
	props:['vmname'],
	template:'<button v-on:click="vm_up">UP</button>',
	methods:{	
			vm_up:function(event){ 
			console.log("VM UP! "+this.vmname);
			fetch('/vm_up/'+this.vmname)
			.then((response)=>{
				if(response.ok){
					return response.text();
				}
				throw new Error('Network response error');
			})
			.then((text)=>{
				console.log(text);
			})
			.catch((error)=>{
				console.log(error);
			});		
			
			},
	},
});

Vue.component('vm_stop',{
	props:['vmname'],
	template:'<button v-on:click="vm_stop">STOP</button>',
	methods:{	
			vm_stop:function(event){ 
			fetch('/vm_stop/'+this.vmname)
			.then((response)=>{
				if(response.ok){
					return response.text();
				}
				throw new Error('Network response error');
			})
			.then((text)=>{
				console.log(text);
			})
			.catch((error)=>{
				console.log(error);
			});		
			
			},
	},
});

Vue.component('vm_status',{
	props:['vmname'],
	data: function(){
		return {status: 'Check status'};
	},
	template:'<button v-on:click="vm_status" >{{ status }}</button>',
	methods:{	
			vm_status:function(event){
			fetch('/vm_status/'+this.vmname)
			.then((response)=>{
				if(response.ok){
					return //console.log(response.text())
				}else{
					return console.log('Status: Indefined');
				}
				throw new Error('Network response error');
			})
			.catch((error)=>{
				console.log(error);
			});		
		},
	},
	created(){
	}
});