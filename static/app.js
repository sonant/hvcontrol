//Vue.use(VueSocketio, "http://localhost:8181/ws");
var app = new Vue({
  el: '#app',
  data: {
	vm_items: [],
	messages: [],
	showModal: false,
  },
  methods:{
	create_vm:function(event){
		this.showModal = true;
		},
	load_data:function(event){
			fetch('/vm_list')
			.then((response)=>{
				if(response.ok){
					return response.json();
				}
				throw new Error('Network response error');
			})
			.then((json)=>{
				console.log(json);
				this.vm_items = json;
			})
			.catch((error)=>{
				console.log(error);
			});		
		}
  },
  created(){
		this.load_data();
	}
});

ws = new WebSocket('ws://'+window.location.hostname+':'+window.location.port+'/ws')
ws.onopen=function(){
	console.log("WS connected");
}
ws.onclose=function(event){
	console.log("WS disconnected "+event.code+event.reason);
}	
ws.onerror=function(error){
	console.log("WS error "+error.message);
}	
ws.onmessage=function(event){
	console.log(event.data);
	app._data.messages.push(event.data);
}		
