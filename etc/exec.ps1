param(
    [string] $cmd,
    [string] $vm_name,
    [string] $mastervhd = "master.vhdx"
)


$User = "vagrant"
$PWord = ConvertTo-SecureString -String "vagrant" -AsPlainText -Force
$LocalCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $PWord
$workdir = ".\vm"


function provision ($LocalCredential,$vm_name){
    Write-Output "Provising - $vm_name ..."
    $vm = Get-Content -Raw -Path vm.json | ConvertFrom-Json 
    Invoke-Command -VMName $vm_name -Credential $LocalCredential -ScriptBlock {
        param($vm_name,$vm_ip, $proxy, $port, $vmproxylogin, $timezone, $language)
        $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
        Set-ItemProperty -Path $reg -Name ProxyServer -Value ([string]::Join(":",$proxy,$port))
        Set-ItemProperty -Path $reg -Name ProxyEnable -Value 1
        $netadapter = Get-NetAdapter 'Ethernet*'
        $netadapter | New-NetIPAddress -IPAddress $vm_ip -PrefixLength "24" -DefaultGateway 192.168.50.1 | Out-Null
        $netadapter | Set-DnsClientServerAddress -ServerAddresses 192.168.50.1 | Out-Null
        Set-TimeZone -Id $timezone
        Rename-LocalUser -Name "vagrant" -NewName $vm_name
        Rename-Computer -NewName ([string]::Join("",$vm_name,"PC")) -Restart
    } -ea SilentlyContinue -ArgumentList $vm_name, $vm.vmip, $vm.vmproxy, $vm.vmproxyport, $vm.vmproxylogin, $vm.timezone, $vm.language
    
}

if($cmd -eq "create"){
    Sleep -Seconds 1
    Write-Output "Copy VHD for VM..."
    Copy-Item ".\etc\$mastervhd" "$workdir\$vm_name\$vm_name.vhdx"
    Write-Output "Copying complete. Creating $vm_name VM..."
    cd "$workdir\$vm_name"
    $VM = @{
      Name = $vm_name
      MemoryStartupBytes = 1GB
      Generation = 1
      VHDPath = (Resolve-Path .\).Path+"\$vm_name.vhdx"
      BootDevice = "VHD"
      Path = (Resolve-Path .\).Path
      SwitchName = (Get-VMSwitch).Name
    }
    if(New-VM @VM){Write-Output "VM creation complete. Starting $vm_name vm..."}
    Start-VM -Name $vm_name
    while ((icm -VMName $vm_name -Credential $LocalCredential {�Test�} -ea SilentlyContinue) -ne �Test�) {Sleep -Seconds 1}
    Write-Output "Configure $vm_name VM..."
    provision $LocalCredential $vm_name
    Write-Output "VM $vm_name ready!!!"
}
if($cmd -eq "getip"){
    Write-Output (icm -VMName $vm_name -Credential $LocalCredential {(Get-NetIPAddress -AddressFamily IPv4).IPAddress})
}

if($cmd -eq "status"){
    $state = (Get-Vm | where {$_.Name -eq $vm_name}).State
    if( $state.length -eq 0){
        Write-Output "$vm_name not found"
    }else{
        Write-Output "$vm_name $state"
        Write-Output (icm -VMName $vm_name -Credential $LocalCredential {(Get-NetIPAddress -AddressFamily IPv4).IPAddress})
    } 
}

if($cmd -eq "start"){
    Start-VM -Name $vm_name
    Write-Output "$vm_name Start"
}
if($cmd -eq "stop"){    
    Stop-VM -Name $vm_name -Force
    Write-Output "$vm_name Stop"
}
if($cmd -eq "destroy"){
    Write-Output "Start removing $vm_name..."
    Stop-VM -Name $vm_name -Force
    Sleep -Seconds 10
    Remove-VM -Name $vm_name -Force
    Sleep -Seconds 10
    Remove-Item "$workdir\$vm_name" -recurse
    Write-Output "Removing $vm_name complete"
}
