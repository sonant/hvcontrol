package main

import (
	"github.com/labstack/echo"
	"net/http"
	"github.com/labstack/gommon/log"
	"encoding/json"
	"os"
	"io/ioutil"
	"os/exec"
	"github.com/labstack/echo/middleware"
	"golang.org/x/net/websocket"
	"bufio"
	"proxy6"
	"strings"
	"strconv"
)
var prx = proxy6.Proxy{""}

type Conf struct {
	Workdir string `json:"workdir"`
	Adress string `json:"address"`
	Iprangestart string `json:"iprangestart"`
}

var (
	conf Conf
 	ch chan string=make(chan string,5)
)

type VmJSON struct {
	Vmname string `json:"vmname"`
	Vmip string `json:"vmip"`
	Vmproxy string `json:"vmproxy"`
	Vmproxyport string `json:"vmproxyport"`
	Vmproxylogin string `json:"vmproxylogin"`
	Vmproxypass string `json:"vmproxypass"`
	Timezone string `json:"timezone"`
	Language string `json:"language"`
}
func cmd(c,vm_name string){
	cmd:=exec.Command("powershell","./etc/exec.ps1", c, vm_name)
	stdout, err :=cmd.StdoutPipe()
	if err != nil {
		log.Print(err.Error())
	}
	scaner:=bufio.NewScanner(stdout)
	go func(){
		for scaner.Scan(){
			ch<-string(scaner.Text())
		}
	}()
	if err := cmd.Start(); err != nil {
		log.Print(err.Error())
	}
	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}
}

func getBlackList()([]string, error) {
	ret := []string{}
	blacklist, err:=os.Open("./blacklist.txt")
	if err!=nil{
		log.Print(err)
		return ret,err
	}
	defer blacklist.Close()
	scanner := bufio.NewScanner(blacklist)
	for scanner.Scan() {
		ret=append(ret,scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Print(err)
		return ret,err
	}
	return ret,nil
}

func get_new_ip() string {
	var status bool
	used_ip_addrs:= getvmlist()
	start,_:=strconv.Atoi(strings.Split(conf.Iprangestart,".")[3])
	for i:= start; i<254; i++{
		status = false
		for k:=range used_ip_addrs{
			uip,_ := strconv.Atoi(strings.Split(used_ip_addrs[k].Vmip,".")[3])
			if i==uip{
				status=true
				break
			}
		}
		if !status {
			ip:=strings.Split(conf.Iprangestart,".")
			ip[3]= strconv.Itoa(i)
			return  strings.Join(ip,".")
		}
	}
	return ""
}

func get_new_proxy() (string,string,string,string){
	var status bool
	available_addrs:= prx.GetProxy()
	blacklist, err:=getBlackList()
	if err!=nil{
		log.Print(err)
	}
	used_proxy_addrs:= getvmlist()
	for i:=range available_addrs{
		status = false
		for k:=range used_proxy_addrs{
			if available_addrs[i].Ip==used_proxy_addrs[k].Vmproxy{
				status=true
				break
			}
		}
		if !status{
			for k:=range  blacklist{
				if available_addrs[i].Ip == blacklist[k]{
					status=true
				}
			}
		}
		if !status {

			return available_addrs[i].Ip,available_addrs[i].Port,available_addrs[i].User,available_addrs[i].Pass
		}
	}
	return "","","",""
}

func main(){
	f, err :=os.OpenFile("log.txt",os.O_WRONLY|os.O_CREATE|os.O_APPEND,0644)
	if err!=nil{
		log.Fatal(err.Error())
	}
	defer f.Close()
	log.SetOutput(f)
	cfg_file, err:=os.Open("./conf.json")
	defer cfg_file.Close()
	if err!=nil{
		log.Print("Conf file read error!")
		os.Exit(1)
	}
	jparser:=json.NewDecoder(cfg_file)
	if err = jparser.Decode(&conf);err!=nil{
		log.Print("Conf file json parse error!")
		os.Exit(1)
	}
	s := echo.New()
	s.Use(middleware.Logger())
	s.Use(middleware.Recover())
	s.Static("/static", "static")
	s.File("/","./index.html")
	s.GET("/vm_create/:vm_name/:timezone/:language",vm_create)
	s.GET("/vm_delete/:vm_name",vm_delete)
	s.GET("/vm_list", vm_list)
	s.GET("/vm_stop", vm_stop)
	s.GET("/vm_getip/:vm_name",getip)
	s.GET("/vm_up/:vm_name",vm_up)
	s.GET("/vm_status/:vm_name",vm_status)
	s.GET("/ws",ws)
	s.HideBanner=true
	s.Logger.SetLevel(log.DEBUG)
	s.Logger.Fatal(s.Start(conf.Adress))
}

func vm_create(c echo.Context) error{
	if _,err:=os.Stat(conf.Workdir + "/" + c.Param("vm_name"));err!=nil{
		if err := os.Mkdir(conf.Workdir + "/" + c.Param("vm_name"), os.ModeDir); err!=nil {
			log.Print("VM dir creation error " + c.Param("vm_name"))
		}
		vmproxy,vmproxyport,vmproxylogin,vmproxypass := get_new_proxy()
		vm:=VmJSON{
			Vmname:c.Param("vm_name"),
			Vmip:get_new_ip(),
			Vmproxy:vmproxy,
			Vmproxyport:vmproxyport,
			Vmproxylogin:vmproxylogin,
			Vmproxypass:vmproxypass,
			Timezone:c.Param("timezone"),
			Language:c.Param("language"),
		}
		vm_json,_:=json.Marshal(vm)
		if err:=ioutil.WriteFile(conf.Workdir + "/" + c.Param("vm_name")+"/vm.json",vm_json,0644);err!=nil{
			log.Print(err.Error())
		}
		go cmd("create", c.Param("vm_name"))
		return c.NoContent(http.StatusOK)
	}
	return c.String(http.StatusOK, "VM already exist:"+c.Param("vm_name"))
}

func ws(c echo.Context) error {
	websocket.Handler(func(ws *websocket.Conn){
		defer ws.Close()
		for {
			err := websocket.Message.Send(ws, <-ch)
			if err != nil {
				c.Logger().Error(err)
				break
			}
		}
	}).ServeHTTP(c.Response(),c.Request())
	return nil
}

func vm_status(c echo.Context) error{
	go cmd("status", c.Param("vm_name"))
	return c.NoContent(http.StatusOK)
}

func vm_up(c echo.Context) error{
	go cmd("up", c.Param("vm_name"))
	return c.NoContent(http.StatusOK)
}

func vm_delete(c echo.Context) error{
	var vm_conf VmJSON
	file, err:=os.Open(conf.Workdir + "/" + c.Param("vm_name") + "/vm.json")
	defer file.Close()
	if err!=nil{
		log.Print(err)
	}
	jparser:=json.NewDecoder(file)
	if err = jparser.Decode(&vm_conf);err!=nil{
		log.Print(err.Error())
	}
	blacklist, err:=os.OpenFile("./blacklist.txt",os.O_APPEND, 644)
	if err!=nil{
		log.Print(err)
	}
	defer blacklist.Close()
	blacklist.WriteString(vm_conf.Vmproxy+"\n")
	go cmd("destroy", c.Param("vm_name"))
	return c.NoContent(http.StatusOK)
}

func getip(c echo.Context) error{
	go cmd("getip", c.Param("vm_name"))
	return c.NoContent(http.StatusOK)
}

func vm_stop(c echo.Context)error{
	go cmd("stop", c.Param("vm_name"))
	return c.NoContent(http.StatusOK)
}

func getvmlist() []VmJSON{
	var vm_conf VmJSON
	vmlist := []VmJSON{}
	files, err := ioutil.ReadDir(conf.Workdir);
	if err != nil {
		log.Print("Work dir read error")
		os.Exit(1)
	}
	for _, f := range files {
		if f.IsDir() {
			vmfile, err:=os.Open(conf.Workdir + "/" + f.Name() + "/vm.json")
			defer vmfile.Close()
			if err!=nil{
				log.Print("VM conf file read error!")
				break
			}
			jparser:=json.NewDecoder(vmfile)
			if err = jparser.Decode(&vm_conf);err!=nil{
				log.Print(err.Error())
			}
			vmlist = append(vmlist,vm_conf)
		}
	}
	return vmlist
}

func vm_list(c echo.Context) error{
	return c.JSON(http.StatusOK, getvmlist())
}
